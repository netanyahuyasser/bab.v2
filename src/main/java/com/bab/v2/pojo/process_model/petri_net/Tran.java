/*
 * 
 * Copyright © 2017-2022 Natanael Yabes Wirawan (yabes.wirawan@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */
package com.bab.v2.pojo.process_model.petri_net;

/**
 * @author Natanael Yabes Wirawan - yabes.wirawan@gmail.com
 *
 */
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Tran {

	private String id;
	private String name;
	private Cond cond;
	private Time time;
	private Code code;
	private Channel channel;
	private String x;
	private String y;
	private Shape_ shape;
	private boolean isSubPage;
	private String idSubPage;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Tran() {
	}

	/**
	 * 
	 * @param id
	 * @param time
	 * @param idSubPage
	 * @param name
	 * @param shape
	 * @param code
	 * @param isSubPage
	 * @param channel
	 * @param cond
	 * @param y
	 * @param x
	 */
	public Tran(String id, String name, Cond cond, Time time, Code code, Channel channel, String x, String y, Shape_ shape, boolean isSubPage, String idSubPage) {
		super();
		this.id = id;
		this.name = name;
		this.cond = cond;
		this.time = time;
		this.code = code;
		this.channel = channel;
		this.x = x;
		this.y = y;
		this.shape = shape;
		this.isSubPage = isSubPage;
		this.idSubPage = idSubPage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Tran withId(String id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Tran withName(String name) {
		this.name = name;
		return this;
	}

	public Cond getCond() {
		return cond;
	}

	public void setCond(Cond cond) {
		this.cond = cond;
	}

	public Tran withCond(Cond cond) {
		this.cond = cond;
		return this;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public Tran withTime(Time time) {
		this.time = time;
		return this;
	}

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

	public Tran withCode(Code code) {
		this.code = code;
		return this;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public Tran withChannel(Channel channel) {
		this.channel = channel;
		return this;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public Tran withX(String x) {
		this.x = x;
		return this;
	}

	public String getY() {
		return y;
	}

	public void setY(String y) {
		this.y = y;
	}

	public Tran withY(String y) {
		this.y = y;
		return this;
	}

	public Shape_ getShape() {
		return shape;
	}

	public void setShape(Shape_ shape) {
		this.shape = shape;
	}

	public Tran withShape(Shape_ shape) {
		this.shape = shape;
		return this;
	}

	public boolean isIsSubPage() {
		return isSubPage;
	}

	public void setIsSubPage(boolean isSubPage) {
		this.isSubPage = isSubPage;
	}

	public Tran withIsSubPage(boolean isSubPage) {
		this.isSubPage = isSubPage;
		return this;
	}

	public String getIdSubPage() {
		return idSubPage;
	}

	public void setIdSubPage(String idSubPage) {
		this.idSubPage = idSubPage;
	}

	public Tran withIdSubPage(String idSubPage) {
		this.idSubPage = idSubPage;
		return this;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public Tran withAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("cond", cond).append("time", time).append("code", code).append("channel", channel).append("x", x).append("y", y).append("shape", shape).append("isSubPage", isSubPage).append("idSubPage", idSubPage).append("additionalProperties", additionalProperties).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(shape).append(code).append(isSubPage).append(cond).append(id).append(time).append(idSubPage).append(additionalProperties).append(name).append(channel).append(y).append(x).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Tran) == false) {
			return false;
		}
		Tran rhs = ((Tran) other);
		return new EqualsBuilder().append(shape, rhs.shape).append(code, rhs.code).append(isSubPage, rhs.isSubPage).append(cond, rhs.cond).append(id, rhs.id).append(time, rhs.time).append(idSubPage, rhs.idSubPage).append(additionalProperties, rhs.additionalProperties).append(name, rhs.name).append(channel, rhs.channel).append(y, rhs.y).append(x, rhs.x).isEquals();
	}

}