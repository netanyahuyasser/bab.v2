/*
 * 
 * Copyright © 2017-2022 Natanael Yabes Wirawan (yabes.wirawan@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */
package com.bab.v2.pojo.process_model.petri_net;

/**
 * @author Natanael Yabes Wirawan - yabes.wirawan@gmail.com
 *
 */
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Place {

	private String id;
	private String name;
	private Mark mark;
	private Color color;
	private String x;
	private String y;
	private Shape shape;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Place() {
	}

	/**
	 * 
	 * @param id
	 * @param color
	 * @param name
	 * @param mark
	 * @param shape
	 * @param y
	 * @param x
	 */
	public Place(String id, String name, Mark mark, Color color, String x, String y, Shape shape) {
		super();
		this.id = id;
		this.name = name;
		this.mark = mark;
		this.color = color;
		this.x = x;
		this.y = y;
		this.shape = shape;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Place withId(String id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Place withName(String name) {
		this.name = name;
		return this;
	}

	public Mark getMark() {
		return mark;
	}

	public void setMark(Mark mark) {
		this.mark = mark;
	}

	public Place withMark(Mark mark) {
		this.mark = mark;
		return this;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Place withColor(Color color) {
		this.color = color;
		return this;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public Place withX(String x) {
		this.x = x;
		return this;
	}

	public String getY() {
		return y;
	}

	public void setY(String y) {
		this.y = y;
	}

	public Place withY(String y) {
		this.y = y;
		return this;
	}

	public Shape getShape() {
		return shape;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	}

	public Place withShape(Shape shape) {
		this.shape = shape;
		return this;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public Place withAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("mark", mark).append("color", color).append("x", x).append("y", y).append("shape", shape).append("additionalProperties", additionalProperties).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(color).append(additionalProperties).append(name).append(mark).append(shape).append(y).append(x).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Place) == false) {
			return false;
		}
		Place rhs = ((Place) other);
		return new EqualsBuilder().append(id, rhs.id).append(color, rhs.color).append(additionalProperties, rhs.additionalProperties).append(name, rhs.name).append(mark, rhs.mark).append(shape, rhs.shape).append(y, rhs.y).append(x, rhs.x).isEquals();
	}

}