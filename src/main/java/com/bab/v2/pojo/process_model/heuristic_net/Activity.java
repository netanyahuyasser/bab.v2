/*
 * 
 * Copyright © 2017-2022 Natanael Yabes Wirawan (yabes.wirawan@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */
package com.bab.v2.pojo.process_model.heuristic_net;

/**
 * @author Natanael Yabes Wirawan - yabes.wirawan@gmail.com
 *
 */
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Activity {

	private String name;
	private double dependency;
	private Duration duration;
	private Frequency frequency;
	private String source;
	private String target;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Activity() {
	}

	/**
	 * 
	 * @param dependency
	 * @param duration
	 * @param source
	 * @param name
	 * @param target
	 * @param frequency
	 */
	public Activity(String name, double dependency, Duration duration, Frequency frequency, String source, String target) {
		super();
		this.name = name;
		this.dependency = dependency;
		this.duration = duration;
		this.frequency = frequency;
		this.source = source;
		this.target = target;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDependency() {
		return dependency;
	}

	public void setDependency(double dependency) {
		this.dependency = dependency;
	}

	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("dependency", dependency).append("duration", duration).append("frequency", frequency).append("source", source).append("target", target).append("additionalProperties", additionalProperties).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(dependency).append(duration).append(source).append(additionalProperties).append(name).append(target).append(frequency).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Activity) == false) {
			return false;
		}
		Activity rhs = ((Activity) other);
		return new EqualsBuilder().append(dependency, rhs.dependency).append(duration, rhs.duration).append(source, rhs.source).append(additionalProperties, rhs.additionalProperties).append(name, rhs.name).append(target, rhs.target).append(frequency, rhs.frequency).isEquals();
	}

}
