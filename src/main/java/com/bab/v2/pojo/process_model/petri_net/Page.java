/*
 * 
 * Copyright © 2017-2022 Natanael Yabes Wirawan (yabes.wirawan@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */
package com.bab.v2.pojo.process_model.petri_net;

/**
 * @author Natanael Yabes Wirawan - yabes.wirawan@gmail.com
 *
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Page {

	private String name;
	private String id;
	private List<Place> places = null;
	private List<Tran> trans = null;
	private List<Arc> arcs = null;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Page() {
	}

	/**
	 * 
	 * @param arcs
	 * @param id
	 * @param trans
	 * @param name
	 * @param places
	 */
	public Page(String name, String id, List<Place> places, List<Tran> trans, List<Arc> arcs) {
		super();
		this.name = name;
		this.id = id;
		this.places = places;
		this.trans = trans;
		this.arcs = arcs;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Page withName(String name) {
		this.name = name;
		return this;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Page withId(String id) {
		this.id = id;
		return this;
	}

	public List<Place> getPlaces() {
		return places;
	}

	public void setPlaces(List<Place> places) {
		this.places = places;
	}

	public Page withPlaces(List<Place> places) {
		this.places = places;
		return this;
	}

	public List<Tran> getTrans() {
		return trans;
	}

	public void setTrans(List<Tran> trans) {
		this.trans = trans;
	}

	public Page withTrans(List<Tran> trans) {
		this.trans = trans;
		return this;
	}

	public List<Arc> getArcs() {
		return arcs;
	}

	public void setArcs(List<Arc> arcs) {
		this.arcs = arcs;
	}

	public Page withArcs(List<Arc> arcs) {
		this.arcs = arcs;
		return this;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public Page withAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("id", id).append("places", places).append("trans", trans).append("arcs", arcs).append("additionalProperties", additionalProperties).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(arcs).append(id).append(trans).append(additionalProperties).append(name).append(places).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Page) == false) {
			return false;
		}
		Page rhs = ((Page) other);
		return new EqualsBuilder().append(arcs, rhs.arcs).append(id, rhs.id).append(trans, rhs.trans).append(additionalProperties, rhs.additionalProperties).append(name, rhs.name).append(places, rhs.places).isEquals();
	}

}