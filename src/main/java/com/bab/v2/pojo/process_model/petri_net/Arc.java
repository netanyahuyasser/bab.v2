/*
 * 
 * Copyright © 2017-2022 Natanael Yabes Wirawan (yabes.wirawan@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */
package com.bab.v2.pojo.process_model.petri_net;

/**
 * @author Natanael Yabes Wirawan - yabes.wirawan@gmail.com
 *
 */
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Arc {

	private String id;
	private String startId;
	private String endId;
	private String orientation;
	private Annot annot;
	private Bendpoint bendpoint;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Arc() {
	}

	/**
	 * 
	 * @param id
	 * @param orientation
	 * @param endId
	 * @param startId
	 * @param bendpoint
	 * @param annot
	 */
	public Arc(String id, String startId, String endId, String orientation, Annot annot, Bendpoint bendpoint) {
		super();
		this.id = id;
		this.startId = startId;
		this.endId = endId;
		this.orientation = orientation;
		this.annot = annot;
		this.bendpoint = bendpoint;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Arc withId(String id) {
		this.id = id;
		return this;
	}

	public String getStartId() {
		return startId;
	}

	public void setStartId(String startId) {
		this.startId = startId;
	}

	public Arc withStartId(String startId) {
		this.startId = startId;
		return this;
	}

	public String getEndId() {
		return endId;
	}

	public void setEndId(String endId) {
		this.endId = endId;
	}

	public Arc withEndId(String endId) {
		this.endId = endId;
		return this;
	}

	public String getOrientation() {
		return orientation;
	}

	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	public Arc withOrientation(String orientation) {
		this.orientation = orientation;
		return this;
	}

	public Annot getAnnot() {
		return annot;
	}

	public void setAnnot(Annot annot) {
		this.annot = annot;
	}

	public Arc withAnnot(Annot annot) {
		this.annot = annot;
		return this;
	}

	public Bendpoint getBendpoint() {
		return bendpoint;
	}

	public void setBendpoint(Bendpoint bendpoint) {
		this.bendpoint = bendpoint;
	}

	public Arc withBendpoint(Bendpoint bendpoint) {
		this.bendpoint = bendpoint;
		return this;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public Arc withAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("startId", startId).append("endId", endId).append("orientation", orientation).append("annot", annot).append("bendpoint", bendpoint).append("additionalProperties", additionalProperties).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(orientation).append(endId).append(startId).append(bendpoint).append(additionalProperties).append(annot).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Arc) == false) {
			return false;
		}
		Arc rhs = ((Arc) other);
		return new EqualsBuilder().append(id, rhs.id).append(orientation, rhs.orientation).append(endId, rhs.endId).append(startId, rhs.startId).append(bendpoint, rhs.bendpoint).append(additionalProperties, rhs.additionalProperties).append(annot, rhs.annot).isEquals();
	}

}