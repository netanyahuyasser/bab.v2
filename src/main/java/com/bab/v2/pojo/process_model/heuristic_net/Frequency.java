/*
 * 
 * Copyright © 2017-2022 Natanael Yabes Wirawan (yabes.wirawan@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */
package com.bab.v2.pojo.process_model.heuristic_net;

/**
 * @author Natanael Yabes Wirawan - yabes.wirawan@gmail.com
 *
 */
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Frequency {

	private long absolute;
	private long cases;
	private long maxRepetition;
	private long relative;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Frequency() {
	}

	/**
	 * 
	 * @param absolute
	 * @param cases
	 * @param relative
	 * @param maxRepetition
	 */
	public Frequency(long absolute, long cases, long maxRepetition, long relative) {
		super();
		this.absolute = absolute;
		this.cases = cases;
		this.maxRepetition = maxRepetition;
		this.relative = relative;
	}

	public long getAbsolute() {
		return absolute;
	}

	public void setAbsolute(long absolute) {
		this.absolute = absolute;
	}

	public long getCases() {
		return cases;
	}

	public void setCases(long cases) {
		this.cases = cases;
	}

	public long getMaxRepetition() {
		return maxRepetition;
	}

	public void setMaxRepetition(long maxRepetition) {
		this.maxRepetition = maxRepetition;
	}

	public long getRelative() {
		return relative;
	}

	public void setRelative(long relative) {
		this.relative = relative;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("absolute", absolute).append("cases", cases).append("maxRepetition", maxRepetition).append("relative", relative).append("additionalProperties", additionalProperties).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(absolute).append(cases).append(additionalProperties).append(relative).append(maxRepetition).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Frequency) == false) {
			return false;
		}
		Frequency rhs = ((Frequency) other);
		return new EqualsBuilder().append(absolute, rhs.absolute).append(cases, rhs.cases).append(additionalProperties, rhs.additionalProperties).append(relative, rhs.relative).append(maxRepetition, rhs.maxRepetition).isEquals();
	}

}