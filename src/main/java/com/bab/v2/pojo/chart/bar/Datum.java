/*
 * 
 * Copyright © 2017-2022 Natanael Yabes Wirawan (yabes.wirawan@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */
package com.bab.v2.pojo.chart.bar;

/**
 * @author Natanael Yabes Wirawan - yabes.wirawan@gmail.com
 *
 */
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Datum {

	private String xAxis;
	private long yAxis;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Datum() {
	}

	/**
	 * 
	 * @param yAxis
	 * @param xAxis
	 */
	public Datum(String xAxis, long yAxis) {
		super();
		this.xAxis = xAxis;
		this.yAxis = yAxis;
	}

	public String getXAxis() {
		return xAxis;
	}

	public void setXAxis(String xAxis) {
		this.xAxis = xAxis;
	}

	public long getYAxis() {
		return yAxis;
	}

	public void setYAxis(long yAxis) {
		this.yAxis = yAxis;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("xAxis", xAxis).append("yAxis", yAxis).append("additionalProperties", additionalProperties).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(yAxis).append(additionalProperties).append(xAxis).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Datum) == false) {
			return false;
		}
		Datum rhs = ((Datum) other);
		return new EqualsBuilder().append(yAxis, rhs.yAxis).append(additionalProperties, rhs.additionalProperties).append(xAxis, rhs.xAxis).isEquals();
	}

}