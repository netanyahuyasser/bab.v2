package com.bab.v2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BabApplication {

	public static void main(String[] args) {
		SpringApplication.run(BabApplication.class, args);
	}
}
